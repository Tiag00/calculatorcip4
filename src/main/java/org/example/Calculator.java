package org.example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class Calculator{
    public int add(int a, int b) { return a+b; }
    public int subtract(int a, int b) {  return a-b;}
    public int multiply(int a, int b) { return a*b; }
    public int divide(int a, int b) { return a/b;}

    public Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public Date subtractDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -(days) );
        return cal.getTime();
    }
    public static long subtractDates(Date date, Date Odate) throws ParseException {
        long difference_In_Time = date.getTime() - Odate.getTime();

        long difference_In_Years = (difference_In_Time / (1000l * 60 * 60 * 24 * 365));

        long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;
        long difference_In_Months = (difference_In_Time / (1000 * 60 * 60 * 24)) % 12;


        return difference_In_Days;

    }

    public static String binaryToHexadecimal(String binary) {
        int decimal = Integer.parseInt(binary, 2);
        return Integer.toHexString(decimal);
    }


    public static String hexadecimalToBinary(String hexadecimal) {
        int decimal = Integer.parseInt(hexadecimal, 16);
        return Integer.toBinaryString(decimal);
    }

    public static int fibonacci(int n) {
        if (n <= 1) {
            return n;
        }
        int a = 0, b = 1;
        for (int i = 2; i <= n; i++) {
            int temp = a + b;
            a = b;
            b = temp;
        }
        return b;
    }

    public static long factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    public static double cylinderVolume(double radius, double height, int decimalPlaces) {
        double volume = Math.PI * Math.pow(radius, 2) * height;
        double multiplier = Math.pow(10, decimalPlaces);
        return Math.round(volume * multiplier) / multiplier;
    }

    public static double coneVolume(double radius, double height, int decimalPlaces) {
        double volume = (1.0 / 3.0) * Math.PI * Math.pow(radius, 2) * height;
        double multiplier = Math.pow(10, decimalPlaces);
        return Math.round(volume * multiplier) / multiplier;
    }
}
