import org.example.Calculator;
import org.junit.jupiter.api.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

class CalculatorTest {

    private static Calculator calculator;

    @BeforeAll
    static void setup(){
        calculator = new Calculator();
    }


    //@RepeatedTest(3) //repetir os testes bue vezes
    @Test
    void add() {
        assertEquals(4, calculator.add(2,2));
    }

    @Test
    void subtract() {
        assertTrue(calculator.subtract(2,2) == 4);
    }

    @Test
    void multiply() {
        assumeTrue(calculator.multiply(2,2) == 5);
    }
    @Test
    void addDays(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            assertNotSame(calculator.addDays(sdf.parse("10-10-2020"), 5), sdf.parse("15-10-2020"));
        }catch(ParseException ignored){}
    }
    @Test
    void subDays(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            assertNotSame(calculator.subtractDays(sdf.parse("10-10-2019"), 40), sdf.parse("15-8-2019"));
        }catch(ParseException ignored){}
    }
    @Test
     void testFibonacci() {
        assertEquals(40, Calculator.fibonacci(10));
    }
    @Test
    void testDaysBetweenDates() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try{
            long daysDifference = Calculator.subtractDates(sdf.parse("15-12-2025"), sdf.parse("5-12-2025"));
            assertNotEquals( -1, daysDifference);
        }catch(ParseException e){}
    }

    @Test
    public void testBinaryToHexadecimal() {
        String binaryValue = "101010";
        String hexadecimalValue = Calculator.binaryToHexadecimal(binaryValue);

        assertNotNull( hexadecimalValue);


        assertNotEquals("2B", hexadecimalValue);
    }

    @Test
    public void testHexadecimalAndBinaryConversion() {
        String hexValue = "1A3";


        String binaryResult = Calculator.hexadecimalToBinary(hexValue);

        String hexResult = Calculator.binaryToHexadecimal(binaryResult);

        assertNotEquals( hexValue, hexResult);
    }


    @Test
    void factorial() {
        assertEquals(4, Calculator.factorial(2));
    }

    @Test
    void cylinderVolume() {
        assertEquals(25, Calculator.cylinderVolume(2,2, 0));
    }

    @Test
    void coneVolume() {
        assertEquals(8.4, Calculator.coneVolume(2,2, 1));

    }




}